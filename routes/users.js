var express = require("express");
var router = express.Router();
const { v4: uuidv4 } = require("uuid");
const { USER_TABLE } = require("../constants/tableNames");
const knex = require("../db");
const { hashPassword, verifyPassword } = require("../utils/passwordHash");
const { errorResponse, successResponse } = require("../utils/response");
const { isEmail, isValidPassword } = require("../utils/validations");
const JWT = require("../utils/jwt");
const { token } = require("morgan");
const jwt = new JWT();
// login
router.get("/login/:username/:password", (req, res) => {
  let {
    params: { username, password },
  } = req;

  if (!username) errorResponse(res, "Email not found");
  if (!password) errorResponse(res, "Password cannot be empty");

  knex(USER_TABLE)
    .where({ email_id: username })
    .select("*")
    .then((record) => {
      if (record.length == 0) errorResponse(res, "Email not registered.");
      if (!verifyPassword(password, record[0].password)) {
        errorResponse(res, "incorrect  password");
        return;
      }

      userData = {
        userId: record[0].user_id,
        userToken: record[0].token,
        fullName: record[0].full_name,
      };

      jwt
        .newToken(userData, record[0].token, req.connection.remoteAddress)
        .then((jwtTokens) => {
          successResponse(
            res,
            { userInfo: userData, token: jwtTokens },
            "data fetched"
          );
        });
    });
});

router.post("/add", async (req, res) => {
  let {
    body: { email, password },
  } = req;

  if (!isEmail(email)) errorResponse(res, "Invalid email ID");
  if (!isValidPassword(password))
    errorResponse(
      res,
      "Password must be 6 to 16 characters and contain 1 alphabet and special character"
    );

  knex(USER_TABLE)
    .where({
      email_id: email,
    })
    .count("* as count")
    .then((count) => {
      console.log(count[0].count);
      if (count[0].count != 0) {
        errorResponse(res, "user already exists");
      }
    });
  let token = uuidv4();
  knex(USER_TABLE)
    .insert({
      email_id: email,
      token,
      password: await hashPassword(password),
    })
    .then((val) => {
      successResponse(res, { userId: val[0], token }, "account registered");
    });
});

router.post("/token", async (req, res) => {
  let token = req.body.token;
  if (!token) errorResponse(res, "Invalid body");

  let newToken = await jwt.resetToken(token);

  if (newToken.err) {
    errorResponse(res, "Something went wrong!", 400, newToken.err);
    return;
  }

  successResponse(res, { token: newToken, issuedOn: Date() });
});

module.exports = router;
