var express = require("express");
var router = express.Router();
const { v4: uuidv4 } = require("uuid");
const knex = require("../db");
const { POSTS_TABLE } = require("../constants/tableNames");
const JWT = require("../utils/jwt");
const { successResponse, errorResponse } = require("../utils/response");
const jwt = new JWT();

router.post("/add", jwt.authenticateToken, (req, res) => {
  let {
    body: { description, user },
  } = req;
  knex(POSTS_TABLE)
    .insert({
      post_token: uuidv4(),
      post_pic: "not supported",
      post_desc: description,
      user_token: user.userToken,
    })
    .then((doc) => {
      knex(POSTS_TABLE)
        .where({ post_id: doc[0] })
        .then((post) => {
          successResponse(res, post, "post added");
        });
    })
    .catch((err) => {
      errorResponse(res, "something went wrong", err);
    });
});

router.get("/", (req, res) => {
  knex(POSTS_TABLE)
    .where({ post_visibility: "public" })
    .select("*")
    .then((posts) => {
      successResponse(res, posts, "posts fetched");
    })
    .catch((err) => {
      errorResponse(res, "something went wrong", 500, err);
    });
});

router.get("/:postToken", (req, res) => {
  knex(POSTS_TABLE)
    .where({ post_id: req.params.postToken })
    .select("*")
    .then((post) => {
      if (post[0].post_visibility == "friends") {
        jwt.authenticateToken(req, res);
      }
      successResponse(res, post[0], "post fetched");
    })
    .catch((err) => {
      errorResponse(res, "something went wrong", 500, err);
    });
});

module.exports = router;
