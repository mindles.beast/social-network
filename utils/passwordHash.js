const bcrypt = require("bcrypt");
const saltRounds = 3;
const hashPassword = (plainPassword) => {
  return new Promise((resolve, reject) => {
    bcrypt.hash(plainPassword, saltRounds, (err, hashedPassword) => {
      if (err) {
        console.error(err);
        reject(err);
      }
      resolve(hashedPassword);
    });
  });
};

const verifyPassword = (password, hashedPassword) => {
  //   return new Promise((resolve, reject) => {
  //     bcrypt
  //       .compare(password, hashedPassword, (err, result) => {
  //         if (err) reject(false);
  //         if (!result) reject(false);
  //         resolve(true);
  //       })
  //       .catch((err) => err);
  //   });

  let val = bcrypt.compareSync(password, hashedPassword);
  console.log({ val });
  return val;
};

module.exports = { hashPassword, verifyPassword };
