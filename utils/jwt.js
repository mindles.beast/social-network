const knex = require("../db");
const dotenv = require("dotenv");
const jwt = require("jsonwebtoken");
dotenv.config();
const { SESSION_TABLE } = require("../constants/tableNames");
const { errorResponse } = require("./response");

class JWT {
  _sign(data) {
    return jwt.sign(data, process.env.jwt_key, { expiresIn: "1h" });
  }
  _verify(token) {
    console.log({ token });
    return jwt.verify(token, process.env.jwt_key, (err, data) => {
      if (err) return null;
      return data;
    });
  }
  _generateResetToken(data, user_token, ip) {
    let token = jwt.sign(data, process.env.jwt_reset_key);
    return knex(SESSION_TABLE)
      .insert({
        user_token: user_token,
        refresh_token: token,
        location: "testing location",
        ip_addr: ip,
      })
      .then((doc) => {
        return token;
      });
  }

  _verifyResetToken(token) {
    return knex(SESSION_TABLE)
      .where({ refresh_token: token })
      .count("* as count")
      .then((count) => {
        if (count[0].count == 0) return { err: "token not found" };
        let user = jwt.verify(token, process.env.jwt_reset_key);
        console.log({ user });
        return this._sign(user);
      })
      .catch((err) => {
        console.log({ err });
        return { err };
      });
  }

  async resetToken(token) {
    return await this._verifyResetToken(token);
  }

  async newToken(data, userToken, ip) {
    return {
      token: this._sign(data),
      refreshToken: await this._generateResetToken(data, userToken, ip),
      createdAt: Date(),
    };
  }

  async authenticateToken(req, res, next) {
    const authorization = req.headers["authorization"];
    const token = authorization && authorization.split(" ")[1];
    if (token == null) {
      errorResponse(res, "token not found", 401);
      return;
    }

    jwt.verify(token, process.env.jwt_key, (err, data) => {
      if (err) {
        errorResponse(res, "Unauthorized", 403, err);
        return;
      }
      req.body.user = data;
      next();
    });
  }
}

module.exports = JWT;
