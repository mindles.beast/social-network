const successResponse = (res, data, msg = "action performed ok") => {
  res.status(200).json({ response: msg, data }).end();
};

const errorResponse = (res, msg, statusCode = 400, errStack = {}) => {
  res.status(statusCode).json({ err: msg, stackTrace: errStack }).end();
};

module.exports = { successResponse, errorResponse };
